'''
Created on 9 Dec 2016

@author: lucyg
'''

import md5

class DayFive(object):

    def __init__(self, params):
        pass
        
input = "abbhdwsy";

def findPasswordPart1(input):
    password = '';
    count = 0
    while len(password) != 8:
        hash = md5.new(input + str(count)).hexdigest()
        if(hash[:5] == "00000"):
            password += hash[5:6]
        count += 1;
    print "Password part 1: ", password;
    
    
def findPasswordPart2(input):
    password = ['','','','','','','',''];
    count = 0;
    while ('' in password):
        hash = md5.new(input + str(count)).hexdigest()
        if(hash[:5] == "00000"):
            index = hash[5:6]
            if(index.isdigit() and int(index) < 8 and password[int(index)] == ''):
                password[int(index)] = hash[6:7]
        count += 1
    passwordStr = ''.join(str(e) for e in password)    
    print "Password part 2: ", passwordStr;
    
findPasswordPart1(input);
findPasswordPart2(input);

