'''
Created on 9 Dec 2016

@author: lucyg
'''

class DaySix(object):

    def __init__(self, params):
        pass

column1 = list();
column2 = list();
column3 = list();
column4 = list();
column5 = list();
column6 = list();
column7 = list();
column8 = list();

def mostCommon(listOfChars):
    count = -1;
    mostCommon = ' ';
    for c in listOfChars:
        if(listOfChars.count(c) > count):
            count = listOfChars.count(c)
            mostCommon = c;
    
    return mostCommon;

def leastCommon(listOfChars):
    count = len(listOfChars);
    leastCommon = ' ';
    for c in listOfChars:
        if(listOfChars.count(c) < count):
            count = listOfChars.count(c)
            leastCommon = c;
    
    return leastCommon;

def part1and2():
    with open("../../../../resources/2016/day6", "r") as f:
        lines = f.readlines()
        for line in lines:
            arrayOfLine = list(line.strip())
            print arrayOfLine
            for index in range(len(arrayOfLine)):
                if(index == 0):
                    column1.append(arrayOfLine[index])
                if(index == 1):
                    column2.append(arrayOfLine[index])
                if(index == 2):
                    column3.append(arrayOfLine[index])
                if(index == 3):
                    column4.append(arrayOfLine[index])
                if(index == 4):
                    column5.append(arrayOfLine[index])
                if(index == 5):
                    column6.append(arrayOfLine[index])
                if(index == 6):
                    column7.append(arrayOfLine[index])
                if(index == 7):
                    column8.append(arrayOfLine[index])                                                            
        a = mostCommon(column1);
        b = mostCommon(column2);
        c = mostCommon(column3);
        d = mostCommon(column4);
        e = mostCommon(column5);
        f = mostCommon(column6);
        g = mostCommon(column7);
        h = mostCommon(column8);
        print "mostCommon part 1:", a + b + c + d + e + f + g + h
        a = leastCommon(column1);
        b = leastCommon(column2);
        c = leastCommon(column3);
        d = leastCommon(column4);
        e = leastCommon(column5);
        f = leastCommon(column6);
        g = leastCommon(column7);
        h = leastCommon(column8);
        print "leastCommon part 1:", a + b + c + d + e + f + g + h
        
        
part1and2();        