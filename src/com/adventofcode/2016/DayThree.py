'''
Created on 9 Dec 2016

@author: lucyg
'''

class DayThree(object):

    def __init__(self, params):
        pass

def isTriangle(sideA, sideB, sideC):
    isATri = True
    if(sideA + sideB <= sideC):
        isATri = False;
    elif(sideA + sideC <= sideB):
        isATri = False;
    elif(sideB + sideC <= sideA):
        isATri = False;
    return isATri;

def part1():
    count = 0
    with open("../../../../resources/2016/day3", "r") as f:
        lines = f.readlines()
        for line in lines:
            sideA, sideB, sideC = line.split()
            print sideA, sideB, sideC
            if(isTriangle(int(sideA), int(sideB), int(sideC))):
                count += 1
        
        print "No. of Triangles: ", count
        
def part2():
    listOfTriangleA = list();
    listOfTriangleB = list();
    listOfTriangleC = list();
    count = 0
    with open("../../../../resources/2016/day3", "r") as f:
        lines = f.readlines()
        for line in lines:
            sideA, sideB, sideC = line.split()
            listOfTriangleA.append(sideA);
            listOfTriangleB.append(sideB);
            listOfTriangleC.append(sideC);
    
    itr = 1
    a = 0
    b = 0
    c = 0
    for side in listOfTriangleA:
        if(itr == 1):
            a = side
            itr += 1
        elif(itr == 2):
            b = side
            itr += 1
        elif(itr == 3):
            c = side
            if(isTriangle(int(a), int(b), int(c))):
                count += 1
            itr = 1   
    for side in listOfTriangleB:
        if(itr == 1):
            a = side
            itr += 1
        elif(itr == 2):
            b = side
            itr += 1
        elif(itr == 3):
            c = side
            if(isTriangle(int(a), int(b), int(c))):
                count += 1
            itr = 1   
    for side in listOfTriangleC:
        if(itr == 1):
            a = side
            itr += 1
        elif(itr == 2):
            b = side
            itr += 1
        elif(itr == 3):
            c = side
            if(isTriangle(int(a), int(b), int(c))):
                count += 1                                
            itr = 1   
        
    print "No. of Triangles: ", count
            
part1();

part2();

    