'''
Created on 9 Dec 2016

@author: glucy
'''

class DayTwo(object):

    def __init__(self, params):
        pass

keypad = [[0, 0, 0, 0, 0], [0, 1, 2, 3, 0], [0, 4, 5, 6, 0], [0, 7, 8, 9, 0], [0, 0, 0, 0, 0]];

#keypad = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 2, 3, 4, 0, 0], [0, 5, 6, 7, 8, 9, 0], [0, 0, 'A', 'B', 'C', 0, 0], [0, 0, 0, 'D', 0,  0, 0], [0, 0, 0, 0, 0, 0, 0]];

def moveUp((upDown, leftRight)):
    if (keypad[upDown - 1][leftRight] != 0):
        return (upDown - 1, leftRight);
    else:
        return (upDown, leftRight);
    
def moveLeft((upDown, leftRight)):
    if (keypad[upDown][leftRight - 1] != 0):
        return (upDown, leftRight - 1);
    else:
        return (upDown, leftRight);    
    
def moveRight((upDown, leftRight)):
    if (keypad[upDown][leftRight + 1] != 0):
        return (upDown, leftRight + 1);
    else:
        return (upDown, leftRight);
        
def moveDown((upDown, leftRight)):
    if (keypad[upDown + 1][leftRight] != 0):
        return (upDown + 1, leftRight);
    else:
        return (upDown, leftRight);
            
def part1():
    upDown = 2;
    leftRight= 2;
    with open("../../../../resources/2016/day2", "r") as f:
        lines = f.readlines();
        for line in lines:
            arrayOfLine = list(line.strip());
            for instruction in arrayOfLine:
                if(instruction == 'U'):
                    (upDown, leftRight) = moveUp((upDown, leftRight));
                elif(instruction == 'L'):
                    (upDown, leftRight) = moveLeft((upDown, leftRight));
                elif(instruction == 'R'):
                    (upDown, leftRight) = moveRight((upDown, leftRight));
                elif(instruction == 'D'):
                    (upDown, leftRight) = moveDown((upDown, leftRight));
            print keypad[upDown][leftRight];

part1();
