'''
Created on 4 Dec 2017

@author: lucyg
'''

class DayFour(object):

    def __init__(self, params):
        pass
    
def part1():
    with open("../../../../resources/2017/day4", "r") as f:
        lines = f.readlines();
        count = 0;
        for line in lines:
            valid = True;
            words = line.split();
            for w in words:
                if words.count(w) > 1:
                    valid = False;
                    break;
            if valid:
                count = count + 1;
                    
        print count;    
    

def part2():
    with open("../../../../resources/2017/day4", "r") as f:
        lines = f.readlines();
        count = 0;
        for line in lines:
            valid = True;
            words = line.split();
            for w in words:
                if words.count(w) > 1:
                    valid = False;
                    break;
                if isAnAnagram(w, words):
                    valid = False;
                    break;
            if valid:
                count = count + 1;
                    
        print count;

def isAnAnagram(w, words):
    listOfSortedWords = [];
    for i in words:
        listOfSortedWords.append(sorted(i));
    sortedWord = sorted(w);
    if listOfSortedWords.count(sortedWord) > 1:
        return True;
    else:
        return False;


part1();
part2();