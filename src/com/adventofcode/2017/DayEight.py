'''
Created on 8 Dec 2017

@author: lucyg
'''

class DayEight():

    def __init__(self):
        pass

register = dict();
    
def part1And2():
    with open("../../../../resources/2017/day8", "r") as f:
        lines = f.readlines();
        largest = 0;
        for line in lines:
            instruction = line.split();
            reg1 = instruction[0];
            incOrDec = instruction[1];
            reg2 = instruction[2];
            conditionReg = instruction[4];
            condition = instruction[5];
            conditionVal = instruction[6];
            
            if reg1 in register:
                # Do Nothing
                print "";
            else:
                register[reg1] = 0;
                
            if isCondition(conditionReg, condition, conditionVal):
                updateRegister(reg1, incOrDec, reg2);
            
            if sortRegisterByValue() > largest:
                largest = sortRegisterByValue();
        
        sortRegisterByValue();
        print "Largest: ", largest;
                
def sortRegisterByValue():
    register_sorted_keys = sorted(register, key=register.get, reverse=False)   
    key = register_sorted_keys[len(register_sorted_keys) - 1];
    print key;
    print register[key];
    return register[key]
            
def updateRegister(reg1, incOrDec, reg2):
    if incOrDec == "inc":
        # Add
        register[reg1] = register.get(reg1) + int(reg2);
    elif incOrDec == "dec":
        # Subtract
        register[reg1] = register.get(reg1) - int(reg2);                
                
def isCondition(conditionReg, condition, conditionVal):
    if condition == "==":
        if int(register.get(conditionReg, 0)) == int(conditionVal):
            return True;
    elif condition == "<":
        if int(register.get(conditionReg, 0)) < int(conditionVal):
            return True;
    elif condition == ">":
        if int(register.get(conditionReg, 0)) > int(conditionVal):
            return True;
    elif condition == "<=":
        if int(register.get(conditionReg, 0)) <= int(conditionVal):
            return True;
    elif condition == ">=":
        if int(register.get(conditionReg, 0)) >= int(conditionVal):
            return True;
    elif condition == "!=":
        if int(register.get(conditionReg, 0)) != int(conditionVal):
            return True;
    else:
        return False;              
                    
        
part1And2();    
            
            