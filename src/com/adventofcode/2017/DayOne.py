'''
Created on 1 Dec 2017

@author: lucyg
'''
class DayOne(object):

    def __init__(self, params):
        pass

def part1():
    with open("../../../../resources/2017/day1", "r") as f:
        lines = f.readlines();
        count = 0;
        total = 0;
        line = lines[0]
        for i in line:
            if count < len(line) - 1: 
                if i == line[count + 1]:
                    total = total + int(line[count + 1]);
                count = count + 1;   
        if line[0] == line[len(line) - 1]:
            total = total + int(line[len(line) - 1]);
    
        print "Total: ", total;

def part2():
    with open("../../../../resources/2017/day1", "r") as f:
        lines = f.readlines();
        count = 0;
        total = 0;
        line = lines[0];
        length = len(line);
        halfway = length / 2;
        for i in line:
            if count < length - 1: 
                print "Count: ", count;
                if (count + halfway < length):
                    if i == line[count + halfway]:
                        total = total + int(line[count + halfway]);
                else:
                    loop = count + halfway - length;
                    if i == line[loop]:
                        total = total + int(line[loop]);
                count = count + 1;
    
        print "Total: ", total;
        
part1();
part2();