#!flask/bin/python
from flask import Flask
from flask import request
from datetime import datetime
from datetime import timedelta

app = Flask(__name__)

answer1 = "38961"; # Pass code on numeric keypad (Day 2 Part 1)
answer2 = "46C92"; # Pass code on modified keypad (Day 2 Part 2)
answer3 = "801b56a7"; # MD5 Pass code (Day 5 Part 1) (Input = abbhdwsy)
answer4 = "424a0197"; # MD5 Pass code (Day 5 Part 2) (Input = abbhdwsy)
errorMsgTooManyAttempts = "Too many attempts in the previous 3mins, please wait and try your answer again shortly ..."
tooManyAnswers = list();

def tooManyAttempts(tooManyAnswers, remoteAddr):
    count = 0;
    for i in tooManyAnswers:
        if (i[0] == remoteAddr) and (i[1] > (datetime.now() - timedelta(minutes=3))):
            count += 1;
    if count >= 3:
        return True;
    else:
        return False
    
def checkAnswerNumeric(realAnswer, request, remoteAddr):
    if 'answer' in request.args:
        answer = request.args['answer'];
        if answer == realAnswer:
            return "Your answer is correct: " + answer + "\n";
        elif answer > realAnswer:
            tooManyAnswers.append((remoteAddr, datetime.now()));
            return "Hint: Your answer is too high. Your guess was: " + answer + "\n";
        elif answer < realAnswer:
            tooManyAnswers.append((remoteAddr, datetime.now()));
            return "Hint: Your answer is too low. Your guess was: " + answer + "\n";

def checkAnswer(realAnswer, request, remoteAddr):
    if 'answer' in request.args:
        answer = request.args['answer'];
        if answer == realAnswer:
            return "Your answer is correct: " + answer + "\n";
        elif answer != realAnswer:
            tooManyAnswers.append((remoteAddr, datetime.now()));
            return "Your answer is incorrect. Your guess was: " + answer + "\n";

def checkAnswerPart3(realAnswer, request, remoteAddr):
    if 'answer' in request.args:
        answer = request.args['answer'];
        if answer == realAnswer:
            result = "Your answer is correct: " + answer + "\n";
            result += "Now go to the reception desk, open the bottom drawer and under the orange high vis jacket you should find your prize";
            return result; 
        elif answer != realAnswer:
            tooManyAnswers.append((remoteAddr, datetime.now()));
            return "Your answer is incorrect. Your guess was: " + answer + "\n";        

@app.route('/')
def index():
    return "Hello World!"

@app.route('/xmascomp', methods=['GET'])
def xmascomp():
    return "The URL you are looking for is: <br/> \n\nhttp://[ip-address]:[port]/xmascomp/[part'n']?answer=[your-answer] <br/> \n\ni.e. http://16.49.191.146:5000/xmascomp/part1?answer=1234"

@app.route('/xmascomp/part1', methods=['GET'])
def api_comp1():
    realAnswer = answer1;
    remoteAddr = request.remote_addr;
    if tooManyAttempts(tooManyAnswers, remoteAddr):
        return errorMsgTooManyAttempts;
    return checkAnswerNumeric(realAnswer, request, remoteAddr);

        
@app.route('/xmascomp/part2', methods=['GET'])
def api_comp2():
    realAnswer = answer2;
    remoteAddr = request.remote_addr;
    if tooManyAttempts(tooManyAnswers, remoteAddr):
        return errorMsgTooManyAttempts;
    return checkAnswer(realAnswer, request, remoteAddr);
        
@app.route('/xmascomp/part3', methods=['GET'])
def api_comp3():
    realAnswer = answer3;
    remoteAddr = request.remote_addr;
    if tooManyAttempts(tooManyAnswers, remoteAddr):
        return errorMsgTooManyAttempts;
    return checkAnswerPart3(realAnswer, request, remoteAddr);
        
@app.route('/xmascomp/part4', methods=['GET'])
def api_comp4():
    realAnswer = answer4;
    remoteAddr = request.remote_addr;
    if tooManyAttempts(tooManyAnswers, remoteAddr):
        return errorMsgTooManyAttempts;
    return checkAnswer(realAnswer, request, remoteAddr);
            

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)
